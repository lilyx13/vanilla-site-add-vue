# Vanilla Website Add Vue README

- [Repository](https://gitlab.com/lilyx13/vanilla-site-add-vue)
- [Gitlab Pages Live Link]()

The goal of this repo is to demonstrate how we can create reusable vue components for things like navigation and footers etc and add them to legacy pages.

This will improve our migration workflow by being able to make our websites and apps have a consistent navigation experience without having to finish porting all of the pages to the new system.

## Outline
- The vue header and footer are added around a bunch of static content on the page
- This system should be able to be used on all of our older sites and apps. 
- The navigation router is configured the same as the one in [this repository](https://gitlab.com/lilyx13/fedora-web-fav-stack)