// Copied from https://gitlab.com/lilyx13/fedora-web-fav-stack/-/blob/main/client/src/router/index.ts with minor edits to set paths
/*

The best way to do this would be to only have to ever edit these links in one place. IF we can serve this globally along with the cdn this could be possible

*/
export const routes = [
  {
    path: "https://getfedora.org",
    name: "Get Fedora",
  },
  {
    path: "https://start.fedoraproject.org",
    name: "Start",
  },
  // Editions Pages
  {
    path: "https://getfedora/workstation",
    name: "Workstation",
  },
  {
    path: "https://getfedora/iot",
    name: "IoT",
  
  },
  {
    path: "https://getfedora/server",
    name: "Server",
  },
  // Emerging Editions Pages
  {
    path: "https://getfedora.org/en/coreos",
    name: "Core OS",
  },
  {
    path: "https://getfedora.org/silverblue",
    name: "Silverblue",
  },
  // Labs
  {
    path: "https://labs.fedoraproject.org/games",
    name: "Games",
  },
  {
    path: "https://labs.fedoraproject.org/scientific",
    name: "Scientific",
  },
  // spins
  {
    path: "https://spins.fedoraproject.org/kde",
    name: "KDE",
  },
  {
    path: "https://spins.fedoraproject.org/xfce",
    name: "XFCE",
  },
];